package ru.ertelecom.k8s.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

@Service
public class ServiceInfo {
    private static final Logger logger = LogManager.getLogger("ServiceInfo");

    public Map<String,String> info () {
        Map<String,String> appInfo=new HashMap<>();
        appInfo.put("appName", "ms-c");
        appInfo.put("appVersion", "v.2");
        return appInfo;
    }

}